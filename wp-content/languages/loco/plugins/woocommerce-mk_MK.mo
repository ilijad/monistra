��    A      $      ,      ,     -     :     J     [     k     ~     �     �     �     �     �     �     �                    *     8  
   ?     J     X     f  	   u          �     �     �     �     �     �     �          %     +     7     =     D     K     _     g     {     �     �     �     �     �     �     �     �  	             (     7  	   @  (   J     s     y     �     �     �     �  	   �  	   �  
   �  �  �  $   �	      �	  !   
  $   '
  !   L
      n
  +   �
      �
     �
  )   �
       O   *      z     �     �     �     �     �     �  !   �  !        ?     T  (   c  (   �     �     �     �     �       8         T     u     �     �  	   �     �  %   �     �            !     >  )   O     y     �     �  -   �     �      �               +     J     W  U   e     �     �     �     �     �  !   �          -     G   Apply coupon Billing address Billing address. Billing details Calculate shipping Cart totals Cart updated. Cash on delivery Category Category: Categories: Checkout Click here to enter your code Continue shopping Date: Description Description (optional) Email address Email: First name Free Shipping Free shipping Have a coupon? Last name Order details Order notes Order number: Page settingCheckout Page settingShop base Page titleCheckout Page titleShop Pay with cash upon delivery. Payment method: Phone Place order Price Price: Prices Proceed to checkout Product Product description Product price. Quantity Related products Reviews Reviews (%d) Settings tab labelCheckout Ship to a different address? Shipping Shipping address Shipping: Short description Street address Subtotal Subtotal: Thank you. Your order has been received. Total Total: Totals Totals. Town / City Update cart View Cart View cart Your order Project-Id-Version: WooCommerce 3.3.1
Report-Msgid-Bugs-To: https://github.com/woocommerce/woocommerce/issues
POT-Creation-Date: 2018-02-06 15:13:44+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-05-06 21:36+0000
Last-Translator: Mitko <admin@podnovi.me>
Language-Team: Македонски јазик
X-Generator: Loco https://localise.biz/
Language: mk_MK
Plural-Forms: nplurals=2; plural=( n % 10 == 1 && n % 100 != 11 ) ? 0 : 1; Искористи купон код Адреса за наплата Адреса за наплата: Податоци за наплата Пресметај достава Вкупно за наплата Кошничката е ажурирана. Плати при достава Категорија Категорија: Категории: Нарачај Кликни овде да го внесеш кодот за промоција Додај во кошничка Датум: Опис Опис (опционално) E-меил адреса E-Меил: Име Бесплатна достава Бесплатна достава Имаш купон? Презиме Податоци за нарачката Забелешка при нарачка Број на нарачка: Нарачај Продавница Нарачај Продавница Плати при подигање на пратката  Начин на плаќање: Телефонски број Изврши нарачка Цена Цена: Цени Комплетирај нарачка Продукт Опис на продуктот Цена на продукт Количина Препорачани производи Оценки Оценки (%d) Нарачај Прати на различна адреса Достава Адреса за достава Достава: Краток опис Улица на живеење Вкупно Вкупно: Ви благодариме. Вашата нарачка е регистрирана. Вкупно Вкупно: Вкупно Вкупно. Град Ажурирај кошничка Кошничка Види кошничка Вашата нарачка 