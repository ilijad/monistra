<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/var/www/html/wp-content/plugins/wp-super-cache/' );
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '7300b070f80292e1dc4d15d489ab4f71ee54c2bef2cfb2b0');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Tz&{[G~2jGb2Ni%2eWl;d%5p#bTNBd-piz>ieRL{}+g9HbrIViJ6xca65Boc{{4c');
define('SECURE_AUTH_KEY',  'r(rOZW:&HC#,.M%SxgZu9n=daVr$.>dL)A/%iwI:`|()6yygqezlsT&H`K6%z]n+');
define('LOGGED_IN_KEY',    ':w|50gx6N=mkNpRm<J*L@n}mtE7`tdIeudsm6bKf(lY/v[>gQ9c~w;)G2-J$m^Vs');
define('NONCE_KEY',        'QYCB#Ez}NKOh7T:Qlv*:_MHEOb;92|Q|~9;_cn`E^dE2%O=&d|=AG&n]XAjx~aFU');
define('AUTH_SALT',        'I<)WYakh7X]+%+Sx^VtNP8KwRab~n@|n<YmEFjo$f&A5Dl(t;ag3pk8qjRjIg)R,');
define('SECURE_AUTH_SALT', 'Azg&3O_m59L[BH;*QScA=khOT{k&nUgIqZgC;_I-(C/3-@Iw</IKjCYNrbL-1{.E');
define('LOGGED_IN_SALT',   'AF+UgfdM{rH<`~!/z820nD&}&Z wIN nMbTvUJ_!y-:VK4V&{mhDAP3#fUh5L*eW');
define('NONCE_SALT',       ';,&OjB]N`p/VAoj+FTmEXJdGMQ)6~*).qHe]vlD.h{eF4]kSWJ/5;Y<o6z!z~7Q#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
